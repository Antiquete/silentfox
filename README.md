# SilentFox Configuration

## Description

A configuration to make firefox silent and stop pinging various locations.

SideNote: Works with other mozilla based browsers too viz. icecat, waterfox etc.

## Installation

Put everything inside "dist" into firefox folder (where executable is).

## Contributions

Any suggestion is welcome. If you want something changed or some new setting added open an issue on git repository. Thanks.